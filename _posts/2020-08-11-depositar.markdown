---
layout: post
title:  "Depositar"
date:   2020-08-11 12:32:14 +0800
categories: data
---

# demo.depositar.io
## 資訊寄存所  
Penghu 澎湖東吉及西吉島上的井位置分佈
<iframe width="700" height="400" src="https://demo.depositar.io/en/dataset/7454f/resource/4d7db2ed-1335-4049-80c0-0c9b31f25f56/view/e207f036-800f-4cba-90fe-9e282c7c5056" frameBorder="0"></iframe>

2019 現存河川水位測量站 shapfile
<iframe width="700" height="400" src="https://demo.depositar.io/en/dataset/river-stage-checkpoint/resource/c0b7a59e-30b3-4a4f-a716-eab752bfe993/view/fc9f7551-07f1-4750-b56d-87ed03c3d370" frameBorder="0"></iframe>

2019 現存河川水位測量站 kml
<iframe width="700" height="400" src="https://demo.depositar.io/en/dataset/river-stage-checkpoint/resource/d7fe675e-1121-4352-9a97-fba4ff79a9e9/view/8c7fd5d6-a52f-4087-b1b4-dfa830253aea" frameBorder="0"></iframe>

2019 現存河川水位測量站 使用QGIS再匯出的kml
<iframe width="700" height="400" src="https://demo.depositar.io/en/dataset/river-stage-checkpoint/resource/973efe8b-9bdc-42e3-a6c0-af19227eebb2/view/5fc1ab44-1130-4b6c-b541-6ae5a3ea5d08" frameBorder="0"></iframe>